import React from "react";

function LastButtons(props) {
  return (
    <div className="lastbtns">
      <div className="tooltip delcompleted">
        <svg
          viewBox="0 0 448 512"
          className="delAll-icon"
          onClick={props.clear}
        >
          <use xlinkHref="/svg-sprite/trash-alt-solid.svg#trash-alt-solid" />
        </svg>
        <span className="tooltiptext">حذف انجام شده ها</span>
      </div>

      <div className="tooltip">
        <svg
          viewBox="0 0 512 512"
          className="resetAll-icon"
          onClick={props.reset}
        >
          <use xlinkHref="/svg-sprite/undo-alt-solid.svg#undo-alt-solid" />
        </svg>
        <span className="tooltiptext">بازیابی همه</span>
      </div>

      <div className="tooltip">
        <svg viewBox="0 0 448 512" className="doAll-icon" onClick={props.done}>
          <use xlinkHref="/svg-sprite/check-square-solid.svg#check-square-solid" />
        </svg>
        <span className="tooltiptext">انجام شدن همه</span>
      </div>

      <div className="tooltip">
        <svg viewBox="0 0 512 512" className="import-icon" onClick={props.get}>
          <use xlinkHref="/svg-sprite/file-import-solid.svg#file-import-solid" />
        </svg>
        <span className="tooltiptext">وارد کردن لیست</span>
      </div>

      <div className="tooltip">
        <svg viewBox="0 0 576 512" className="export-icon" onClick={props.set}>
          <use xlinkHref="/svg-sprite/file-export-solid.svg#file-export-solid" />
        </svg>
        <span className="tooltiptext">خروجی لیست</span>
      </div>
    </div>
  );
}

export default LastButtons;
