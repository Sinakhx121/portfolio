import React from "react";

function List(props) {
  const fullList = props.tasks.map(
    (item, index) =>
      (props.completed ? item.done : !item.done) && (
        <li key={index}>
          <div className="task-orders">
            <div className="order-btns">
              <div className="tooltip">
                <svg
                  viewBox="0 0 320 512"
                  className="move-icons"
                  onClick={() => {
                    props.onMove(item, index, 'down');
                  }}
                >
                  <use xlinkHref="/svg-sprite/angle-down-solid.svg#angle-down-solid" />
                </svg>
                <span className="tooltiptext ttp4">پایین آوردن</span>
              </div>

              <div className="tooltip">
                <svg
                  viewBox="0 0 320 512"
                  className="move-icons"
                  onClick={() => {
                    props.onMove(item, index, 'up');
                  }}
                >
                  <use xlinkHref="/svg-sprite/angle-up-solid.svg#angle-up-solid" />
                </svg>
                <span className="tooltiptext ttp5">بالا بردن</span>
              </div>
            </div>
            <div className="list-item">{item.task}</div>
          </div>
          <div className="list-btns">
            <div className="tooltip">
              <svg
                viewBox={item.done ? "0 0 512 512" : "0 0 448 512"}
                className={item.done ? "restore" : "done"}
                onClick={() => {
                  props.onDo(item, index, !item.done);
                }}
              >
                {item.done ? (
                  <use xlinkHref="/svg-sprite/undo-alt-solid.svg#undo-alt-solid" />
                ) : (
                  <use xlinkHref="/svg-sprite/check-square-solid.svg#check-square-solid" />
                )}
              </svg>

              {item.done ? (
                <span className="tooltiptext ttp1">بازیابی</span>
              ) : (
                <span className="tooltiptext ttp1">انجام شدن</span>
              )}
            </div>

            <div className="tooltip">
              <svg
                viewBox="0 0 576 512"
                className="edit-icon"
                onClick={() => {
                  props.onEdit(item, index);
                }}
              >
                <use xlinkHref="/svg-sprite/edit-solid.svg#edit-solid" />
              </svg>
              <span className="tooltiptext ttp2">ویرایش</span>
            </div>

            <div className="tooltip">
              <svg
                viewBox="0 0 448 512"
                className="delete-icon"
                onClick={() => {
                  props.onDelete(item);
                }}
              >
                <use xlinkHref="/svg-sprite/trash-alt-solid.svg#trash-alt-solid" />
              </svg>
              <span className="tooltiptext ttp3">حذف کردن</span>
            </div>
          </div>
        </li>
      )
  );
  const classStyle = props.completed ? "DoneList" : "TodoList";

  return <ul className={classStyle}>{fullList}</ul>;
}

export default List;
