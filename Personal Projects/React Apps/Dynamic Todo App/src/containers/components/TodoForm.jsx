import React from "react";
import useInputState from "./customHooks/useInputState";

function TodoForm({ addtodo }) {
  const [value, handleChange, reset] = useInputState("");
  const handleEnterKey = e => {
    const code = e.keyCode || e.which;
    if (code === 13) {
      value && addtodo(value);
      reset();
    }
  };
  return (
    <div className="form">
      <input
        type="text"
        id="inp"
        value={value}
        onChange={handleChange}
        onKeyPress={handleEnterKey}
      />

      <div className="form-btn">
        <div className="tooltip">
          <svg
            viewBox="0 0 448 512"
            className="Add-icon"
            onClick={() => {
              value && addtodo(value);
              reset();
            }}
          >
            <use xlinkHref="/svg-sprite/plus-square-solid.svg#plus-square-solid" />
          </svg>
          <span className="tooltiptext">اضافه کردن</span>
        </div>

        <div className="tooltip">
          <svg
            viewBox="0 0 512 512"
            className="cancel-icon"
            onClick={() => {
              reset();
            }}
          >
            <use xlinkHref="/svg-sprite/window-close-solid.svg#window-close-solid" />
          </svg>
          <span className="tooltiptext">انصراف</span>
        </div>
      </div>
    </div>
  );
}

export default TodoForm;
