import React, { useState, useEffect } from "react";
import "../assets/css/styles.css";

function Timer(props) {
  const [timer, setTimer] = useState(0);
  const [interval, _setInterval] = useState(true);
  const [butt, setButt] = useState("Stop");
  const [laps, setLaps] = useState([]);

  const handleButt = () => {
    if (butt === "Start") {
      _setInterval(
        setInterval(() => {
          setTimer(time => time + 1);
        }, 10)
      );
      setButt("Stop");
    } else {
      clearInterval(interval);
      setButt("Start");
    }
  };

  const handleReset = () => {
    clearInterval(interval);
    setTimer(0);
    setLaps([]);
    setButt("Start");
  };

  const handleLap = () => {
    if (butt === "Stop") setLaps(laps.concat(runningTime));
  };

  useEffect(() => {
    _setInterval(
      setInterval(() => {
        setTimer(time => time + 1);
      }, 10)
    );
  }, []);

  function show(time) {
    return time < 10 ? `0${time}` : time;
  }

  const ms = timer % 100;
  const sec = Math.floor(timer / 100) % 60;
  const min = Math.floor(timer / 6000) % 60;
  const hr = Math.floor(timer / 360000);
  const fake = Math.floor(Math.random() * 10);
  let ns = fake - Math.floor(fake / 10) * 10;
  ns = +ms || +sec || +min || +hr ? ns : 0;

  const runningTime = `${show(hr)}:${show(min)}:${show(sec)}:${show(ms)}${ns}`;

  return (
    <div id="container">
      <div className="main">
        <div className="buttons">
          <button id="reset" onClick={handleReset}>
            Reset
          </button>
          <button id="start" onClick={handleButt}>
            {butt}
          </button>
        </div>
        <div className="timer">{runningTime}</div>
        <div className="buttons">
          <button id="lap" onClick={handleLap}>
            Lap
          </button>
        </div>
      </div>
      <ol className="laps">
        {laps.map((lap, index) => {
          return (
            <li key={lap}>
              <span id="lapNum">Lap {index + 1}.</span> {lap}
            </li>
          );
        })}
      </ol>
    </div>
  );
}

export default Timer;
