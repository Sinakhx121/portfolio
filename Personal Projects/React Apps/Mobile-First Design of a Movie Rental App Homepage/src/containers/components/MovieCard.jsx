import React, { Component } from "react";
import { StarsInactive } from "../assets/svg/StarsInactive";

class MovieCard extends Component {
  render() {
    return (
      <div>
        <img
          src={this.props.image}
          alt="movie"
          style={{ width: `${this.props.width}` }}
        />
        <div className="movie-title">{this.props.title}</div>
        {this.props.stars && (
          <div className="desc">
            <span>{this.props.comments}</span>&nbsp;
            <span>نظر </span>
            <StarsInactive />
          </div>
        )}
      </div>
    );
  }
}

export default MovieCard;
