import React from "react";
import { Form, Select, Input, Button } from 'antd';
import DatePicker from 'react-datepicker2';

const { Option } = Select;

class Form1 extends React.Component {

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleSelectChange = value => {
    console.log(value);
    this.props.form.setFieldsValue({
      note: `Hi, ${value === 'male' ? 'man' : 'lady'}!`,
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form labelCol={{ span: 10 }} wrapperCol={{ span: 14 }} onSubmit={this.handleSubmit}>

        <Form.Item label="نام">
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'لطفاً نام خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="نام خانوادگی">
          {getFieldDecorator('lastname', {
            rules: [{ required: true, message: 'لطفاً نام خانوادگی خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="نام پدر">
          {getFieldDecorator('dadname', {
            rules: [{ required: true, message: 'لطفاً نام پدر خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="شماره شناسنامه">
          {getFieldDecorator('idnumber', {
            rules: [{ required: true, message: 'لطفاً شماره شناسنامه خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="کد ملی">
          {getFieldDecorator('nationalNumber', {
            rules: [{ required: true, message: 'لطفاً کد ملی خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="تاریخ تولد">
          {getFieldDecorator('birthdate', {
            rules: [{ required: true, message: 'لطفاً تاریخ تولد خود را وارد نمایید' }],
          })(<DatePicker 
            timePicker={false}
            isGregorian={false}
           />)}
        </Form.Item>

        <Form.Item label="وضعیت تأهل" className="marriage">
          {getFieldDecorator('marriageStatus', {
            rules: [{ required: true, message: 'لطفاً وضعیت تأهل خود را انتخاب نمایید' }],
          })(
            <Select
              placeholder=""
              onChange={this.handleSelectChange}
            >
              <Option value="male">مجرد</Option>
              <Option value="female">متأهل</Option>
            </Select>,
          )}
        </Form.Item>

        <Form.Item label="تعداد فرزند">
          {getFieldDecorator('kids', {
            rules: [{ required: true, message: 'لطفاً تعداد فرزند خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="شغل همسر">
          {getFieldDecorator('spouseJob', {
            rules: [{ required: true, message: 'لطفاً شغل همسر خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="محل کار همسر">
          {getFieldDecorator('spouseWorkplace', {
            rules: [{ required: true, message: 'لطفاً محل کار همسر خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="میزان تحصیلات">
          {getFieldDecorator('education', {
            rules: [{ required: true, message: 'لطفاً میزان تحصیلات خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="رشته تحصیلی">
          {getFieldDecorator('studyField', {
            rules: [{ required: true, message: 'لطفاً رشته تحصیلی خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="نام دانشگاه">
          {getFieldDecorator('university', {
            rules: [{ required: true, message: 'لطفاً نام دانشگاه خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="محل سکونت">
          {getFieldDecorator('residanceLocation', {
            rules: [{ required: true, message: 'لطفاً محل سکونت خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="تلفن">
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'لطفاً تلفن خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="تلفن همراه">
          {getFieldDecorator('mobile', {
            rules: [{ required: true, message: 'لطفاً تلفن همراه خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="محل کار فعلی پدر">
          {getFieldDecorator('dadsworkplace', {
            rules: [{ required: true, message: 'لطفاً محل کار فعلی پدر خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        <Form.Item label="تلفن">
          {getFieldDecorator('dadPhone', {
            rules: [{ required: true, message: 'لطفاً تلفن پدر خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>
        
        <Form.Item label="شماره تماس ضروری">
          {getFieldDecorator('emergencyCall', {
            rules: [{ required: true, message: 'لطفاً شماره تماس ضروری خود را وارد نمایید' }],
          })(<Input />)}
        </Form.Item>

        { false &&
          <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
          <Button type="primary" htmlType="submit" >
            ثبت
          </Button>
        </Form.Item>}
      </Form>
    );
  }
}

const FormInput = Form.create({ name: 'coordinated' })(Form1);

export default FormInput;