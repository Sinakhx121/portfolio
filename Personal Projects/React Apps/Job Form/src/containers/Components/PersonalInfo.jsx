import React, { Component } from "react";
import { Row, Col, Input, Button } from "antd";
import FormInput from "./FormInput";
import LastJobs from "./LastJobs";
import LinearRadioBtns from "./LinearRadioBtns";
import Softwares from "./Softwares";
import TextArea from "antd/lib/input/TextArea";

class PersonalInfo extends Component {
  render() {
    return (
      <div>
        <Row className="personal-info">
          <Col span={24}>
            <FormInput />
          </Col>
        </Row>

        <div className="form-title">سوابق شغلی</div>
        <Row className="personal-info">
          <Col span={24}>
            <LastJobs />
          </Col>
        </Row>

        <div className="form-title smalli">
          <span>آدرس، تلفن و نام مسئول آخرین محل کار:‌</span>
          <Input className="smallt" placeholder="آدرس، تلفن و نام مسئول" />
        </div>

        <div className="form-title">
          <span>میزان آشنایی کلی با ابزارها و مفاهیم برنامه نویسی:‌</span>
          <LinearRadioBtns />
        </div>

        <Row className="personal-info">
          <Col span={24}>
            <Softwares />
          </Col>
        </Row>

        <div className="form-title">
          <span className="smallt">میزان آشنایی با Git:‌</span>
          <LinearRadioBtns />
        </div>

        <div className="form-title">
          <span className="smallt">میزان آشنایی با API:‌</span>
          <LinearRadioBtns />
        </div>

        <div className="form-title">
          <span className="smallt">میزان آشنایی با LINUX:‌</span>
          <LinearRadioBtns />
        </div>

        <Row>
          <Col className="form-title" span={24}>
            <TextArea
              placeholder="آنچه از برنامه نویسی می دانید را در قالب چند جمله بنویسید"
              rows={4}
            />
          </Col>
        </Row>

        <Row>
          <Col className="form-title" span={24}>
            <TextArea
              placeholder="خلاصه اطلاعات لازم در خصوص سوابق شغلی"
              rows={4}
            />
          </Col>
        </Row>

        <Row>
          <Col className="form-title" span={24}>
            <TextArea
              placeholder="چه مناطقی از تهران و کرج را می شناسید و میزان آشنایی شما چقدر است؟"
              rows={4}
            />
          </Col>
        </Row>

        <div className="form-title smalli">
          <span>میزان حقوق پیشنهادی:‌</span>
          <Input className="smallt" placeholder="میزان حقوق پیشنهادی (به تومان)" />
        </div>

        <Row>
          <Col span={24} className="copyright">
          <Button type="primary">ثبت و ارسال</Button>
          </Col>
        </Row>

        <Row>
          <Col span={24} className="copyright">Copyright &copy; Sina Khodabandehloo - 2019</Col>
        </Row>
      </div>
    );
  }
}

export default PersonalInfo;
